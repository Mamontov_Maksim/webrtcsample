package me.amryousef

import com.google.gson.Gson
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.http.cio.websocket.send
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.WebSocketServerSession
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import java.lang.Exception
import java.time.Duration
import java.util.*

fun main(args: Array<String>) {

    val s = embeddedServer(Netty, port = 8080, host = "10.10.11.210") {

        install(DefaultHeaders) {
            header("X-Engine", "Ktor") // will send this header with each response
        }

        install(CallLogging)

        install(WebSockets) {
            pingPeriod = Duration.ofSeconds(15)
            timeout = Duration.ofSeconds(15)
            maxFrameSize = Long.MAX_VALUE
            masking = false
        }

        install(ContentNegotiation) {
            gson {
            }
        }

        val gson = Gson()

        val connections = Collections.synchronizedMap(mutableMapOf<String, WebSocketServerSession>())

        val users = mutableListOf<User>()

        routing {

            webSocket(path = PATH_SIGN_IN) {
                var userName: String = ""
                for (data in incoming) {
                    if (data is Frame.Text) {
                        gson.fromJson(data.readText(), User::class.java).takeIf { it.userName != null }?.let {
                            println("connected user {${it.userName}}")
                            users.add(it)
                            userName = it.userName
                            connections[it.userName] = this
                        }

                        gson.fromJson(data.readText(), Message::class.java).takeIf {
                            it.message != null && it.userReceiver != null && it.userSender != null
                        }?.let { messager ->
                            val userName = messager.userReceiver.userName

                            connections[userName].also {
                                println(
                                    "Sending to ${userName.substring(0, 8)} " +
                                            "from ${messager.userSender.userName.substring(0, 8)} ," +
                                            " message ${messager.message}"
                                )
                                it?.send(data)
                            }
                        }
                    }
                }

                println("disconnect : $userName")
                connections.remove(userName)
                users.remove(User(userName))
            }

            webSocket(path = PATH_CHANGE_RESOLUTION) {
                //Fix
                for (data in incoming) {
                    if (data is Frame.Text) {
                        val user = gson.fromJson(data.readText(), User::class.java)
                        val resolution = user.resolution

                        val currentUser = users.filter { it.userName != user.userName }

                        currentUser.forEach {
                            it.resolution = resolution
                            send(gson.toJson(it))
                        }
                    }
                }
            }

            webSocket(path = PATH_GET_USERS_CONNECTED) {
                for (data in incoming) {
                    val userName = (data as Frame.Text).readText()
                    val jsonUsers = gson.toJson(users.filter { it.userName != userName })
                    println("getUsers: users {$jsonUsers}")
                    send(jsonUsers)
                }
            }
        }
    }
    s.start(wait = true)
}

data class User(val userName: String, var resolution: Resolution? = null)

data class Resolution(val width: Int, val height: Int, val fps: Int = 30)

data class Message(val userSender: User, val userReceiver: User, val message: String)

const val PATH_SIGN_IN = "/route/path/to/ws/signIn"
const val PATH_CHANGE_RESOLUTION = "/route/path/to/ws/changeResolution"
const val PATH_GET_USERS_CONNECTED = "/route/path/to/ws/getUsers"
