package org.webrtc

import org.webrtc.CameraSession.FailureType


internal interface Events : CameraSession.Events

internal interface CreateSessionCallback : CameraSession.CreateSessionCallback