package com.nix.sample.webrtc.ui.dialog

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.nix.sample.webrtc.ui.MessageInterface
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Provider

class AlertMessageInterface @Inject constructor(activityProvider: Provider<Activity>) :
    MessageInterface(activityProvider) {

    private var alertDialog: AlertDialog? = null

    override fun showMessage(
        activity: Activity,
        message: String,
        title: String,
        confirmButtonResId: Int?,
        cancelButtonResId: Int?,
        confirmationListener: (() -> Unit)?,
        cancelListener: (() -> Unit)?
    ) {
        if (alertDialog?.isShowing == true) {
            Timber.w("Alert dialog is already shown. Skip message: title=$title;message=$message")
            return
        }

        val builder = AlertDialog.Builder(activity)
            .setTitle(title)
            .setMessage(message)
            .setOnDismissListener { alertDialog = null }

        confirmButtonResId?.let {
            builder.setPositiveButton(it) { _, _ -> confirmationListener?.invoke() }
        }

        cancelButtonResId?.let {
            builder.setNegativeButton(it) { _, _ -> cancelListener?.invoke() }
        }

        alertDialog = builder.show()
    }
}
