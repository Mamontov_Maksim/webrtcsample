package com.nix.sample.webrtc.webrtc.listener

import com.nix.sample.webrtc.mvp.entity.User
import org.webrtc.IceCandidate
import org.webrtc.SessionDescription
import java.lang.Exception

interface SignallingClientListener {

    fun onConnectionEstablished()

    fun onOfferReceived(description: SessionDescription, userSender: User)

    fun onAnswerReceived(description: SessionDescription, userSender: User)

    fun onIceCandidateReceived(iceCandidate: IceCandidate)

    fun onFailureConnect(exception: Exception)

    fun onGetUsers(users: List<User>)

    fun onDisconnect(userSender: User)
}
