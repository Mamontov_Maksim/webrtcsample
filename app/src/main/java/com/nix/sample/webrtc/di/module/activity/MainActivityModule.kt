package com.nix.sample.webrtc.di.module.activity

import android.app.Activity
import com.nix.sample.webrtc.di.scope.ActivityScope
import com.nix.sample.webrtc.mvp.contract.MainActivityContract
import com.nix.sample.webrtc.mvp.presenter.MainPresenter
import com.nix.sample.webrtc.ui.activity.MainActivity
import com.nix.sample.webrtc.ui.MessageInterface
import com.nix.sample.webrtc.ui.dialog.AlertMessageInterface
import com.nix.sample.webrtc.ui.navigation.BaseNavigator
import com.nix.sample.webrtc.ui.navigation.MainActivityNavigator
import dagger.Binds
import dagger.Module

@Module interface MainActivityModule {

    @Binds @ActivityScope
    fun bindAlertMessageInterface(messageInterface: AlertMessageInterface): MessageInterface

    @Binds @ActivityScope
    fun bindNavigator(navigator: MainActivityNavigator): BaseNavigator

    @Binds @ActivityScope
    fun bindPresenter(presenter: MainPresenter): MainActivityContract.Presenter

    @Binds @ActivityScope
    fun bindActivity(activity: MainActivity): Activity
}
