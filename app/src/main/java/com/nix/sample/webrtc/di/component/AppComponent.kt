package com.nix.sample.webrtc.di.component

import android.content.Context
import com.nix.sample.webrtc.Application
import com.nix.sample.webrtc.di.module.ActivitiesProvider
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivitiesProvider::class
    ]
)
interface AppComponent : AndroidInjector<Application> {

    @Component.Builder interface Builder {

        @BindsInstance fun context(context: Context): Builder

        fun build(): AppComponent
    }
}