package com.nix.sample.webrtc.webrtc.camera

data class Camera(
    val name: String,
    val widthList: List<Int>,
    val heightList: List<Int>,
    val fpsList: List<Int>,
    val resolution: List<String>
)