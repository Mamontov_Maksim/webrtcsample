package com.nix.sample.webrtc.mvp.repository.server.contract

import com.nix.sample.webrtc.mvp.entity.Message
import com.nix.sample.webrtc.mvp.entity.User
import java.lang.Exception

interface ServerRepositoryContract {

    fun sendMessage(message: Message)

    fun signIn(localUser: User)

    suspend fun getUsers(localUser: User): List<User>

    interface GetUsersObserver {

        fun onGetUsersSuccess(users: List<User>)

        fun onGetUsersFailed(exception: Exception)
    }
}