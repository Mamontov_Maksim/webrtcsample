package com.nix.sample.webrtc.mvp

import androidx.annotation.CallSuper
import com.nix.sample.webrtc.BuildConfig.DEBUG
import com.nix.sample.webrtc.mvp.exception.ViewNotAttachedException
import io.reactivex.Scheduler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import ru.terrakok.cicerone.Router
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

/**
 * Base class for all presenters.
 *
 * @param <V> view related for presenter implementation.
 */
// TODO: 1/9/19 find solution to prevent double data loading after screen rotation
abstract class BasePresenter<V : BaseContract.View> protected constructor(
    val router: Router
) : BaseContract.Presenter<V>,
    CoroutineScope {

    override val coroutineContext = Job() + Dispatchers.Main

    @Suppress("ConstantConditionIf")
    final override var view: V? = null
        get() = field ?: field.apply {
            if (DEBUG) throw ViewNotAttachedException() else Timber.e("View is not attached")
        }
        private set

    override fun onError(throwable: Throwable) {
        if (runCatching { view?.onError(throwable) }.getOrNull() == null) {
            Timber.e(throwable, "Can't show error, view is not attached")
        }
    }

    @CallSuper override fun attachView(view: V) {
        this.view = view
        onAttach()
    }

    @CallSuper override fun detachView() {
        this.view = null
        onDetach()
    }

    override fun onAttach() {
        //none
    }

    override fun onDetach() {
        //none
    }
}