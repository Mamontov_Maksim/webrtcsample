package com.nix.sample.webrtc.ui

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.nix.sample.webrtc.mvp.BaseContract
import com.nix.sample.webrtc.ui.navigation.BaseNavigator
import com.nix.sample.webrtc.ui.toolbar.DefaultToolbarInterface
import dagger.android.support.DaggerAppCompatActivity
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Inject

abstract class BaseActivity<V : BaseContract.View, P : BaseContract.Presenter<V>> :
    DaggerAppCompatActivity(),
    BaseContract.View {

    @Inject protected lateinit var presenter: P

    @Inject protected lateinit var navigatorHolder: NavigatorHolder

    @Inject protected lateinit var navigator: BaseNavigator

    @Inject protected lateinit var messageInterface: MessageInterface

    @Inject lateinit var toolbarInterface: DefaultToolbarInterface
        protected set

    @get:LayoutRes protected abstract val layoutId: Int

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        presenter.attachView(this as V)
    }

    override fun onResume() {
        super.onResume()
        invalidateToolbar()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun onError(throwable: Throwable) {
        messageInterface.showError(throwable)
    }

    open fun getToolbarInterface(): ToolbarInterface = toolbarInterface

    protected open fun invalidateToolbar() {
        getToolbarInterface().invalidate()
    }

    override fun showProgress() {
        // TODO: 10/06/2019  show progress dialog
    }

    override fun hideProgress() {
        // TODO: 10/06/2019  hide progress dialog
    }
}