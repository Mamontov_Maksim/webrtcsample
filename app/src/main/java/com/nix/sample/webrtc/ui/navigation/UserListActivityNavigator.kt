package com.nix.sample.webrtc.ui.navigation

import com.nix.sample.webrtc.ui.activity.UserListActivity
import javax.inject.Inject

class UserListActivityNavigator @Inject constructor(activity: UserListActivity) : BaseNavigator(
    activity
)
