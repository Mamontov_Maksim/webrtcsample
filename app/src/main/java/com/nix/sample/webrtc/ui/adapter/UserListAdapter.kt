package com.nix.sample.webrtc.ui.adapter

import android.view.View
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.mvp.entity.User
import kotlinx.android.synthetic.main.item_user.view.userName

class UserListAdapter(val onUserClick: (User) -> Unit) : BaseAdapter<User>() {

    override fun getLayoutId(viewType: Int) = R.layout.item_user

    override fun onCreateViewHolder(view: View, viewType: Int) = UserListHolder(view, onUserClick)

    class UserListHolder(view: View, val onUserClick: (User) -> Unit) : BaseViewHolder<User>(view) {

        private val userTextView = view.userName

        override fun bind(item: User) {
            super.bind(item)

            itemView.setOnClickListener { onUserClick(item) }
            userTextView.text = item.userName
        }
    }
}