package com.nix.sample.webrtc.mvp.exception

open class BaseException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
