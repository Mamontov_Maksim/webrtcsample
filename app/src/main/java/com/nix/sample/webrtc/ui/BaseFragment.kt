package com.nix.sample.webrtc.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import com.nix.sample.webrtc.mvp.BaseContract
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject

abstract class BaseFragment<V : BaseContract.View, P : BaseContract.Presenter<V>> :
    DaggerFragment(),
    BaseContract.View {

    @Inject protected lateinit var presenter: P

    protected val titleString: String get() = requireContext().getString(titleResId)

    @get:LayoutRes protected abstract val layoutId: Int

    @get:StringRes protected abstract val titleResId: Int

    @get:MenuRes protected open val menuRes: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutId, container, false)

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menuRes?.let { inflater?.inflate(it, menu) }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this as V)
    }

    override fun onResume() {
        super.onResume()
        invalidateToolbar()
    }

    override fun onDestroyView() {
        presenter.detachView()
        super.onDestroyView()
    }

    final override fun onError(throwable: Throwable) {
        getActivityIfFragmentVisible()?.onError(throwable)
    }

    override fun showProgress() {
        getActivityIfFragmentVisible()?.showProgress()
    }

    override fun hideProgress() {
        getActivityIfFragmentVisible()?.hideProgress()
    }

    open fun getToolbarInterface(): ToolbarInterface? = getBaseActivity()?.getToolbarInterface()

    protected fun invalidateToolbar() {
        getToolbarInterface()?.apply {
            invalidate()
            setTitle(titleString)
        } ?: Timber.d("ToolbarInterface is null")

        setHasOptionsMenu(menuRes != null && menuRes!! > 0)
    }

    protected fun getBaseActivity() = activity as? BaseActivity<*, *>

    private fun getActivityIfFragmentVisible() =
        getBaseActivity()
            ?.takeIf { isVisible }
            .also { if (it == null) Timber.d("Activity is null or fragment is not visible") }
}