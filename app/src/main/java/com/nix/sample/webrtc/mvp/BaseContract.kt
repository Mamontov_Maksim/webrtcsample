package com.nix.sample.webrtc.mvp

interface BaseContract {

    interface View {

        /**
         * Default method to show error. Will be called by presenter if got error.
         *
         * @param throwable instance of error
         */
        fun onError(throwable: Throwable)

        /**
         * Method will be called on long played operations. Show default progress.
         */
        fun showProgress()

        /**
         * Method will be called on long played operation was finished. Hide default progress.
         */
        fun hideProgress()
    }

    interface Presenter<V : View> {

        /**
         * View related to presenter implementation.
         * For debug build general scenario should throw [ViewNotAttachedException]
         * in case if view is not attached, i.e. view is null.
         * For release build in this case error is logged without exception throwing .
         */
        val view: V?

        /**
         * This method should be called from presenter implementation for handling error.
         * It would help us to implement error handling.
         *
         * If need show information about error an appropriate method in [View] must be declared.
         *
         * @param throwable instance of error
         */
        fun onError(throwable: Throwable)

        /**
         * Should be called when view is ready for receiving data from presenter.
         * Best place to call this method:
         * - [androidx.fragment.app.Fragment.onViewCreated]
         * - [android.app.Activity.onCreate]
         *
         * @param view instance of [View]
         */
        fun attachView(view: V)

        /**
         * Should be called when view is unable to receive more data.
         * Best place to call this method:
         * - [androidx.fragment.app.Fragment.onDestroyView]
         * - [android.app.Activity.onDestroy]
         */
        fun detachView()

        fun onAttach()

        fun onDetach()
    }
}
