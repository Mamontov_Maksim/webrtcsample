package com.nix.sample.webrtc.mvp.entity

data class Message(var userReceiver: User, var userSender: User, var message: String = "") {

    companion object {

        fun emptyMessage(userReceiver: User, userSender: User) = Message(userReceiver, userSender)
    }
}