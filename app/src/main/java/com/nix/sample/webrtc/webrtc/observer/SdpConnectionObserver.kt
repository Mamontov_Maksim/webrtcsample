package com.nix.sample.webrtc.webrtc.observer

import org.webrtc.SdpObserver
import org.webrtc.SessionDescription
import timber.log.Timber

open class SdpConnectionObserver : SdpObserver {

    override fun onSetFailure(exception: String?) {
        Timber.d("onSetFailure : $exception")
    }

    override fun onSetSuccess() {
        Timber.d("on set Success")
    }

    override fun onCreateSuccess(description: SessionDescription?) {
        Timber.d("onCreateSuccess")
    }

    override fun onCreateFailure(exception: String?) {
        Timber.d("onCreateFailure : $exception")
    }
}