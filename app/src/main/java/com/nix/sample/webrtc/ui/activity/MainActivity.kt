package com.nix.sample.webrtc.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.mvp.contract.MainActivityContract
import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.ui.BaseActivity
import com.nix.sample.webrtc.ui.adapter.UserAdapter
import com.nix.sample.webrtc.webrtc.client.PeerConnectionClient
import kotlinx.android.synthetic.main.activity_main.callFAB
import kotlinx.android.synthetic.main.activity_main.cameraResolutionSpinner
import kotlinx.android.synthetic.main.activity_main.constraintLayout
import kotlinx.android.synthetic.main.activity_main.dropCallFAB
import kotlinx.android.synthetic.main.activity_main.mainSurfaceView
import kotlinx.android.synthetic.main.activity_main.progressBar
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.activity_main.switchCamera
import kotlinx.android.synthetic.main.activity_main.userNameTextView
import org.webrtc.SurfaceViewRenderer

class MainActivity :
    BaseActivity<MainActivityContract.View, MainActivityContract.Presenter>(),
    MainActivityContract.View {

    override val layoutId = R.layout.activity_main

    private lateinit var resolutionAdapter: ArrayAdapter<String>
    private lateinit var userAdapter: UserAdapter

    private val onItemSelectedListener =
        object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                resolutionAdapter.getItem(position)?.let {
                    presenter.onCameraResolutionSelected(it)
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        userAdapter = UserAdapter(
            { surfaceView, position ->
                presenter.onRemoteSurfaceViewClick(surfaceView, position)
            },
            { presenter.initSurfaceView(it) }
        )
        super.onCreate(savedInstanceState)



        recyclerView.adapter = userAdapter
        recyclerView.layoutManager = GridLayoutManager(this, 1)

        callFAB.setOnClickListener { presenter.onCallClick() }

        dropCallFAB.setOnClickListener { presenter.onDropCallClick() }

        switchCamera.setOnClickListener { presenter.onSwitchCameraClick() }

        cameraResolutionSpinner.onItemSelectedListener = onItemSelectedListener
    }

    override fun addHideUser(user: User) {
        userAdapter.setHideUser(user)
    }

    override fun getMainSurfaceView(): SurfaceViewRenderer = mainSurfaceView

    override fun addUser(user: User) {
        userAdapter.addUser(user)
    }

    override fun getUsers() = userAdapter.getUsers()

    override fun getUser(position: Int) = userAdapter.getUser(position)

    override fun deleteHideUser() = userAdapter.deleteHideUser()

    override fun getRemoteSurfaceViewList() = userAdapter.getSurfaceViews()

    override fun getRemoteSurfaceView(position: Int) = userAdapter.getSurfaceView(position)

    override fun getSurfaceViewByUser(user: User) = userAdapter.getSurfaceViewByUser(user)

    override fun replaceUser(userPosition: Int, user: User) = userAdapter.replaceUserByPosition(userPosition, user)

    override fun getUserPosition(user: User) = userAdapter.getUserPosition(user)

    override fun replaceAllUser(userList: List<User>) = userAdapter.replaceUsers(userList)

    override fun setCameraResolutionListSpinner(resolutionList: List<String>) {
        ArrayAdapter(this, android.R.layout.simple_spinner_item, resolutionList).run {
            resolutionAdapter = this
            cameraResolutionSpinner.adapter = resolutionAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        toolbarInterface.setVisible(false)
    }

    override fun setCallFABVisibility(visible: Boolean) {
        callFAB.isVisible = visible
    }

    override fun hideProgress() {
        progressBar.isVisible = false
    }

    override fun showProgress() {
        progressBar.isVisible = true
    }

    override fun successfulConnected() {
        Snackbar.make(constraintLayout, R.string.successful_connected, Snackbar.LENGTH_SHORT).show()
    }

    override fun setUserNameText(userName: String) {
        userNameTextView.text = userName.subSequence(0, 8)
    }

    override fun onError(throwable: Throwable) {
        Snackbar.make(
            constraintLayout,
            getString(R.string.error_conect) + throwable.localizedMessage,
            Snackbar.LENGTH_SHORT
        ).show()
    }

    override fun showUserListActivityForResult(userList: ArrayList<User>) {
        val intent = Intent(this, UserListActivity::class.java).run {
            putParcelableArrayListExtra(UserListActivity.USER_LIST_KEY, userList)
        }
        startActivityForResult(intent, USER_LIST_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode and resultCode) {

            USER_LIST_REQUEST_CODE, Activity.RESULT_OK -> {
                data?.getParcelableExtra<User>(USER_RESULT_KEY)?.let {
                    presenter.callUser(it)
                }
            }
        }
    }

    companion object {
        const val USER_LIST_REQUEST_CODE = 0

        const val USER_RESULT_KEY = "USER_RESULT_KEY"
    }
}