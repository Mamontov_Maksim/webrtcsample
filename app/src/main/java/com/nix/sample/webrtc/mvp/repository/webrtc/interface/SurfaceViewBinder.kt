package com.nix.sample.webrtc.mvp.repository.webrtc.`interface`

import com.nix.sample.webrtc.mvp.entity.User
import org.webrtc.SurfaceViewRenderer

interface SurfaceViewBinder {

    fun addSurfaceView(user: User, surfaceView: SurfaceViewRenderer)
}