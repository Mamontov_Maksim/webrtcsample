package com.nix.sample.webrtc.ui

import android.app.Activity
import androidx.annotation.StringRes
import com.nix.sample.webrtc.BuildConfig
import com.nix.sample.webrtc.R
import javax.inject.Provider

// TODO: 21/06/2019 Check if there is a leak when using just Activity without Provider<Activity>
// https://gitlab.nixdev.co/android-research/mvppattern-kotlin/issues/42
abstract class MessageInterface(protected val activityProvider: Provider<Activity>) {

    fun showError(
        @StringRes messageResId: Int,
        throwable: Throwable? = null,
        @StringRes titleResId: Int = R.string.error_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(it.getString(messageResId), throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showError(
        @StringRes messageResId: Int,
        throwable: Throwable? = null,
        title: String,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(it.getString(messageResId), throwable),
            title = title,
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showError(
        message: String,
        throwable: Throwable? = null,
        @StringRes titleResId: Int = R.string.error_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(message, throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showError(
        message: String,
        throwable: Throwable? = null,
        title: String,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(message, throwable),
            title = title,
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showError(
        throwable: Throwable,
        @StringRes titleResId: Int = R.string.error_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = createMessageFromThrowable(throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showError(
        throwable: Throwable,
        title: String,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = createMessageFromThrowable(throwable),
            title = title,
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showInfo(
        @StringRes messageResId: Int,
        throwable: Throwable? = null,
        @StringRes titleResId: Int = R.string.info_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(it.getString(messageResId), throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showInfo(
        @StringRes messageResId: Int,
        throwable: Throwable? = null,
        title: String,
        @StringRes confirmButtonResId: Int = R.string.ok,
        @StringRes cancelButtonResId: Int = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(it.getString(messageResId), throwable),
            title = title,
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showInfo(
        message: String,
        throwable: Throwable? = null,
        @StringRes titleResId: Int = R.string.info_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(message, throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showInfo(
        message: String,
        throwable: Throwable? = null,
        title: String,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(message, throwable),
            title = title,
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showInfo(
        throwable: Throwable,
        @StringRes titleResId: Int = R.string.info_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = createMessageFromThrowable(throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showInfo(
        throwable: Throwable,
        title: String,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = createMessageFromThrowable(throwable),
            title = title,
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    protected abstract fun showMessage(
        activity: Activity,
        message: String,
        title: String,
        @StringRes confirmButtonResId: Int?,
        @StringRes cancelButtonResId: Int?,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    )

    private fun createMessageFromThrowable(throwable: Throwable) = buildString {
        append(throwable::class.simpleName)
        throwable.message?.takeUnless(String::isEmpty)?.run { append(": $this") }
    }

    private fun buildMessage(message: String, throwable: Throwable?) = buildString {
        append(message)
        if (BuildConfig.DEBUG) {
            throwable?.let {
                append('\n')
                append(createMessageFromThrowable(it))
            }
        }
    }
}
