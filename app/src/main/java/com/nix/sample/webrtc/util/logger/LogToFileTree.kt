package com.nix.sample.webrtc.util.logger

import android.content.Context
import android.util.Log
import com.nix.sample.webrtc.BuildConfig
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.util.logging.FileHandler
import java.util.logging.Level
import java.util.logging.LogRecord

class LogToFileTree(context: Context) : Timber.Tree() {

    private val internalStorageCache: File = context.cacheDir

    private var fileHandler = FileHandler(
        internalStorageCache.toString() + File.separator + LOG_FILES_TEMPLATE,
        SIZE_LIMIT_BYTES,
        FILES_COUNT,
        true
    )

    override fun log(
        priority: Int,
        tag: String?,
        message: String,
        throwable: Throwable?
    ) = writeMessageToFile(getLogLevel(priority), message)

    private fun getLogLevel(priority: Int) =
        when (priority) {
            Log.ERROR -> Level.SEVERE
            Log.WARN -> Level.WARNING
            else -> Level.INFO
        }

    private fun writeMessageToFile(level: Level, message: String) {
        try {
            fileHandler.publish(LogRecord(level, message))
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    companion object {

        // TODO: 2019-06-10 remove JvmStatic after convert Screens to kotlin
        @JvmStatic val LOG_FILE_NAME = BuildConfig.APPLICATION_ID

        private val LOG_FILES_TEMPLATE = "$LOG_FILE_NAME%g.log"

        private const val SIZE_LIMIT_BYTES = 1024 * 504

        private const val FILES_COUNT = 2
    }
}
