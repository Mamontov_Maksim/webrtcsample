package com.nix.sample.webrtc.ui.adapter

import android.view.View
import androidx.core.view.isVisible
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.mvp.entity.User
import kotlinx.android.synthetic.main.item_surface_view.view.surfaceRenderView
import kotlinx.android.synthetic.main.item_surface_view.view.userName
import org.webrtc.SurfaceViewRenderer

class UserAdapter(
    private val onSurfaceViewClick: (SurfaceViewRenderer, Int) -> Unit,
    private val initSurfaceView: (SurfaceViewRenderer) -> Unit
) : BaseAdapter<User>() {

    private val surfaceViewList = mutableListOf<SurfaceViewRenderer>()

    private val userList = mutableListOf<User>()
    var userHide: User? = null

    override fun getLayoutId(viewType: Int) = R.layout.item_surface_view

    override fun onCreateViewHolder(view: View, viewType: Int) =
        UserHolder(view, onSurfaceViewClick, initSurfaceView)

    fun addUser(user: User) {
        userList.add(user)
        notifyDataSetChanged()
    }

    fun replaceUsers(userList: List<User>) {
        this.userList.removeAll(this.userList)
        this.userList.addAll(userList)
    }

    fun getUser(position: Int) = userList[position]

    fun getUsers() = userList

    fun getUserPosition(user: User) = userList.indexOf(user)

    fun replaceUserByPosition(userPosition: Int, user: User) {
        userList[userPosition] = user
        notifyDataSetChanged()
    }

    fun setHideUser(user: User) {
        userHide = user
        notifyDataSetChanged()
    }

    fun deleteHideUser() {
        userHide = null
        notifyDataSetChanged()
    }

    fun getSurfaceViews() = surfaceViewList

    fun getSurfaceViewByUser(user: User): SurfaceViewRenderer {
        val position = userList.indexOf(user)
        if (position >= 0) {
            return getSurfaceView(position)
        } else {
            return getSurfaceView(0)
        }
    }

    fun getSurfaceView(position: Int) = surfaceViewList[position]

    override fun onBindViewHolder(holder: BaseViewHolder<User>, position: Int) {
        holder.bind(userList[position])
    }

    override fun getItemCount() = userList.size

    inner class UserHolder(
        itemView: View,
        onSurfaceViewClick: (SurfaceViewRenderer, Int) -> Unit,
        initSurfaceView: (SurfaceViewRenderer) -> Unit
    ) : BaseViewHolder<User>(itemView) {

        private val surfaceView = itemView.surfaceRenderView
        private val userNameTextView = itemView.userName

        init {
            surfaceViewList.add(surfaceView)
            surfaceView.setOnClickListener {
                onSurfaceViewClick(surfaceView, surfaceViewList.indexOf(surfaceView))
            }
            initSurfaceView(surfaceView)
        }

        override fun bind(item: User) {
            super.bind(item)

            itemView.isVisible = userHide != item
            userNameTextView.text =
                if (item.userName.length >= 8)
                    "${item.userName.subSequence(0, 8)}.."
                else
                    item.userName
        }
    }
}