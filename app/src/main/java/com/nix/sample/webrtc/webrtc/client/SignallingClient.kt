package com.nix.sample.webrtc.webrtc.client

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.nix.sample.webrtc.mvp.entity.Message
import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.mvp.repository.server.contract.ServerRepositoryContract
import com.nix.sample.webrtc.mvp.repository.server.contract.ServerRepositoryContract.GetUsersObserver
import com.nix.sample.webrtc.webrtc.listener.SignallingClientListener
import io.ktor.client.HttpClient
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.websocket.WebSockets
import io.ktor.client.features.websocket.ws
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.receiveOrNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.webrtc.IceCandidate
import org.webrtc.SessionDescription
import timber.log.Timber
import java.lang.reflect.Type

class SignallingClient(
    private val listener: SignallingClientListener
) : CoroutineScope {

    private val job = Job()
    private val gson = Gson()
    private val sendChannel = ConflatedBroadcastChannel<Message>()
    private val client = HttpClient {
        install(WebSockets)
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
    }

    override val coroutineContext = job + Dispatchers.IO

    fun signIn(localUser: User) {
        launch {
            try {
                client.ws(host = HOST_ADDRESS, port = PORT, path = PATH_SIGN_IN) {

                    listener.onConnectionEstablished()

                    val frameText = Frame.Text(gson.toJson(localUser))

                    outgoing.send(frameText)

                    val sendData = sendChannel.openSubscription()

                    while (true) {

                        sendData.poll()?.let {
                            val message = gson.toJson(it)
                            Timber.d("Sending message $message")
                            outgoing.send(Frame.Text(message))
                        }

                        incoming.poll()?.let { frame ->
                            if (frame is Frame.Text) {
                                val data = frame.readText()
                                Timber.d("Received: $data")
                                val messager = gson.fromJson(data, Message::class.java)
                                val userSender = messager.userSender
                                val jsonObject =
                                    gson.fromJson(messager.message, JsonObject::class.java)
                                if (messager.message == MESSAGE_DISCONNECT) {
                                    listener.onDisconnect(userSender)
                                }
                                withContext(Dispatchers.Main) {
                                    if (jsonObject.has("serverUrl")) {
                                        setUrlIceCandidate(jsonObject)
                                    } else if (jsonObject.has("type") && jsonObject.get("type").asString == "OFFER") {
                                        setOfferDescription(jsonObject, userSender)
                                    } else if (jsonObject.has("type") && jsonObject.get("type").asString == "ANSWER") {
                                        setAnswerDescription(jsonObject, userSender)
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (exception: Exception) {
                listener.onFailureConnect(exception)
            }
        }
    }

    fun getUsers(localUser: User, observer: GetUsersObserver) {
        launch {
            try {
                client.ws(host = HOST_ADDRESS, port = PORT, path = PATH_GET_USERS_CONNECTED) {

                    outgoing.send(Frame.Text(localUser.userName))

                    incoming.receiveOrNull()?.let { frame ->

                        val itemsListType: Type = object : TypeToken<ArrayList<User>>() {}.type
                        val data = (frame as Frame.Text).readText()
                        val users = gson.fromJson<ArrayList<User>>(data, itemsListType)

                        if (users is ArrayList<User>){
                            Timber.d("getUsers userList $users")
                            observer.onGetUsersSuccess(users)
                        } else {
                            observer.onGetUsersFailed(Exception("didn't get users"))
                        }

                    }
                }
            } catch (exception: Exception) {
                observer.onGetUsersFailed(exception)
            }
        }
    }

    fun send(dataObject: Message) = runBlocking {
        sendChannel.send(dataObject)
    }

    fun destroy() {
        client.close()
        job.complete()
    }

    private fun setUrlIceCandidate(jsonObject: JsonObject) {
        listener.onIceCandidateReceived(gson.fromJson(jsonObject, IceCandidate::class.java))
    }

    private fun setOfferDescription(jsonObject: JsonObject, userSender: User) {
        listener.onOfferReceived(
            gson.fromJson(jsonObject, SessionDescription::class.java),
            userSender
        )
    }

    private fun setAnswerDescription(jsonObject: JsonObject, userSender: User) {
        listener.onAnswerReceived(
            gson.fromJson(jsonObject, SessionDescription::class.java),
            userSender
        )
    }

    companion object {
        const val HOST_ADDRESS = "10.10.11.210"
        const val PORT = 8080

        const val MESSAGE_DISCONNECT = "disconnect"

        const val PATH_SIGN_IN = "/route/path/to/ws/signIn"
        const val PATH_GET_USERS_CONNECTED = "/route/path/to/ws/getUsers"
    }
}