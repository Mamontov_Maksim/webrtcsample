package com.nix.sample.webrtc.mvp.contract

import com.nix.sample.webrtc.mvp.BaseContract
import com.nix.sample.webrtc.mvp.entity.User
import org.webrtc.SurfaceViewRenderer

interface MainActivityContract {

    interface View : BaseContract.View {

        fun successfulConnected()

        fun setCallFABVisibility(visible: Boolean)

        fun showUserListActivityForResult(userList: ArrayList<User>)

        fun setUserNameText(userName: String)

        fun getMainSurfaceView(): SurfaceViewRenderer?

        fun getRemoteSurfaceView(position: Int): SurfaceViewRenderer

        fun getRemoteSurfaceViewList(): List<SurfaceViewRenderer>

        fun setCameraResolutionListSpinner(resolutionList: List<String>)

        fun addUser(user: User)

        fun addHideUser(user: User)

        fun getUser(position: Int): User

        fun deleteHideUser()

        fun getSurfaceViewByUser(user: User): SurfaceViewRenderer

        fun replaceUser(userPosition: Int, user: User)

        fun getUserPosition(user: User): Int

        fun replaceAllUser(userList: List<User>)
        fun getUsers(): MutableList<User>
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onCallClick()

        fun callUser(user: User)

        fun onSwitchCameraClick()

        fun onCameraResolutionSelected(resolution: String)

        fun initSurfaceView(surfaceView: SurfaceViewRenderer)

        fun onRemoteSurfaceViewClick(surfaceView: SurfaceViewRenderer, peerConnectionId: Int)

        fun onDropCallClick()
    }
}