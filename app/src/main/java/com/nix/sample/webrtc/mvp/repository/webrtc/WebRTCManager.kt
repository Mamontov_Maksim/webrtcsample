package com.nix.sample.webrtc.mvp.repository.webrtc

import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.mvp.repository.webrtc.`interface`.SurfaceViewBinder
import com.nix.sample.webrtc.webrtc.client.PeerConnect
import com.nix.sample.webrtc.webrtc.client.PeerConnectionClient2
import com.nix.sample.webrtc.webrtc.observer.PeerConnectionObserver
import org.webrtc.SurfaceViewRenderer
import org.webrtc.VideoTrack
import javax.inject.Inject

class WebRTCManager @Inject constructor(
    private val peerClient: PeerConnectionClient2,
    private val mainSurfaceView: SurfaceViewRenderer
) : SurfaceViewBinder {

    private val userHashMap = hashMapOf<User, SurfaceViewRenderer>()
    private val peerConnectList = mutableListOf<PeerConnect>()

    init {
        peerClient.startLocalVideoCapture(mainSurfaceView)
    }

    override fun addSurfaceView(user: User, surfaceView: SurfaceViewRenderer) {
        userHashMap[user] = surfaceView
    }

    private fun addPeerConnection(user: User, peerConnectionObserver: PeerConnectionObserver) {
        val peerConnection = peerClient.newPeerConnection(peerConnectionObserver)
        peerConnectList.add(PeerConnect(peerConnection, user))
    }

    private fun setRemoteVideoTrack(peerConnectionPosition: Int, videoTrack: VideoTrack) {
        peerConnectList[peerConnectionPosition].videoTrack = videoTrack
    }
}