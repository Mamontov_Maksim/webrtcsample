package com.nix.sample.webrtc.di.module.rtc

import android.content.Context
import com.nix.sample.webrtc.webrtc.client.PeerConnectionClient
import dagger.Module
import dagger.Provides

@Module class WebRTCModule {

    @Provides
    fun peerConnectionClientProvider(context: Context) = PeerConnectionClient(context)
}