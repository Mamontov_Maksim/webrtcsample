package com.nix.sample.webrtc.ui.navigation

import com.nix.sample.webrtc.ui.activity.MainActivity
import javax.inject.Inject

class MainActivityNavigator @Inject constructor(activity: MainActivity) : BaseNavigator(activity)
