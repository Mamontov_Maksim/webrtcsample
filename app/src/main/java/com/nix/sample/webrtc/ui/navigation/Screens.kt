package com.nix.sample.webrtc.ui.navigation

import com.nix.sample.webrtc.ui.activity.UserListActivity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.nix.sample.webrtc.ui.activity.MainActivity
import com.nix.sample.webrtc.util.extensions.isIntentSafe
import com.nix.sample.webrtc.util.io.getPathToLogFile
import com.nix.sample.webrtc.util.media.MimeType
import com.nix.sample.webrtc.util.system.MIUIUtils
import ru.terrakok.cicerone.android.support.SupportAppScreen
import timber.log.Timber

class Screens {

    class UserListScreen : SupportAppScreen() {

        override fun getActivityIntent(context: Context) =
            Intent(context, UserListActivity::class.java)
    }

    class MainScreen : SupportAppScreen() {

        override fun getActivityIntent(context: Context) =
            Intent(context, MainActivity::class.java)
    }

    class SettingsPermissionsScreen : SupportAppScreen() {

        override fun getActivityIntent(context: Context): Intent? {
            val miui8AndHigherIntent = MIUIUtils.createMIUI8AndHigherIntent(context)
            val miuiLessThan8Intent = MIUIUtils.createMIUILessThan8Intent(context)
            val defaultSettingsIntent = createAppDetailsScreenIntent(context)

            return when {
                context.isIntentSafe(miui8AndHigherIntent) ->
                    miui8AndHigherIntent.also { Timber.d("Settings screen for MIUI >= 8 is used") }

                context.isIntentSafe(miuiLessThan8Intent) ->
                    miuiLessThan8Intent.also { Timber.d("Settings screen for MIUI < 8 is used") }

                context.isIntentSafe(defaultSettingsIntent) ->
                    defaultSettingsIntent.also { Timber.d("Standard settings screen is used") }

                else -> {
                    Timber.e("Can't find settings activity")
                    null
                }
            }
        }

        private fun createAppDetailsScreenIntent(context: Context) =
            Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                .apply {
                    data = Uri.fromParts(PACKAGE_URI_SCHEME, context.packageName, null)
                }

        companion object {

            private const val PACKAGE_URI_SCHEME = "package"
        }
    }

    class SendMailWithLogScreen : SupportAppScreen() {

        override fun getActivityIntent(context: Context): Intent {
            val logFilesPath = context.getPathToLogFile()

            return Intent(Intent.ACTION_SEND_MULTIPLE)
                .setType(MimeType.PLAIN_TEXT)
                .putParcelableArrayListExtra(
                    Intent.EXTRA_STREAM,
                    ArrayList<Uri>(logFilesPath)
                )
                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }
}
