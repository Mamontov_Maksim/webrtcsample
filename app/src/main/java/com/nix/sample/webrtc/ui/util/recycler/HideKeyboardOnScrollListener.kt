package com.nix.sample.webrtc.ui.util.recycler

import androidx.recyclerview.widget.RecyclerView
import com.nix.sample.webrtc.util.extensions.hideKeyboard

class HideKeyboardOnScrollListener : RecyclerView.OnScrollListener() {

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        if (RecyclerView.SCROLL_STATE_DRAGGING == newState) recyclerView.hideKeyboard()
    }
}
