package com.nix.sample.webrtc.webrtc.camera

import android.content.Context
import org.webrtc.Camera2Capturer
import org.webrtc.CameraVideoCapturer
import java.lang.Exception

class VideoCapturerHelper(
    context: Context,
    cameraName: String?,
    handler: CameraVideoCapturer.CameraEventsHandler?
) : Camera2Capturer(context, cameraName, handler) {

    fun getCurrentCameraName() = cameraName ?: throw Exception("didn't found camera name")

    override fun startCapture(width: Int, height: Int, framerate: Int) {
        super.startCapture(width, height, framerate)
    }

    override fun stopCapture() {
        super.stopCapture()
    }

    override fun switchCamera(switchEventsHandler: CameraVideoCapturer.CameraSwitchHandler?) {
        super.switchCamera(switchEventsHandler)
    }
}