package com.nix.sample.webrtc.mvp.exception

class HandledException(message: String, cause: Throwable) : BaseException(message, cause)
