package com.nix.sample.webrtc.mvp.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(val userName: String) : Parcelable