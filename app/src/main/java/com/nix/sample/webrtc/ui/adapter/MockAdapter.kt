package com.nix.sample.webrtc.ui.adapter

import android.view.View
import android.widget.TextView
import com.nix.sample.webrtc.R
import kotlinx.android.synthetic.main.mock_list_item.view.mockTextView

class MockAdapter : BaseAdapter<String>() {

    init {
        val data = (1..99).map { "item #$it" }.toList()
        replaceItems(data)
    }

    override fun getLayoutId(viewType: Int) = R.layout.mock_list_item

    override fun onCreateViewHolder(view: View, viewType: Int) = StringHolder(view)

    inner class StringHolder(itemView: View) : BaseViewHolder<String>(itemView) {

        private val mockTextView: TextView = itemView.mockTextView

        override fun bind(item: String) {
            super.bind(item)
            mockTextView.text = item
        }
    }
}