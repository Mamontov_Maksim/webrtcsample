package com.nix.sample.webrtc.mvp.repository.server

import com.nix.sample.webrtc.mvp.entity.Message
import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.mvp.repository.server.contract.ServerRepositoryContract
import com.nix.sample.webrtc.webrtc.client.SignallingClient
import javax.inject.Inject
import kotlin.coroutines.suspendCoroutine

class ServerRepository @Inject constructor(
    private val signallingClient: SignallingClient
) : ServerRepositoryContract {

    override fun signIn(localUser: User) {
        signallingClient.signIn(localUser)
    }

    override fun sendMessage(message: Message) {
        signallingClient.send(message)
    }

    override suspend fun getUsers(localUser: User) = suspendCoroutine<List<User>> {
        signallingClient.getUsers(localUser, UsersObserver(it))
    }
}
