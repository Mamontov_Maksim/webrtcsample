package com.nix.sample.webrtc.webrtc.observer

import org.webrtc.DataChannel
import org.webrtc.IceCandidate
import org.webrtc.MediaStream
import org.webrtc.PeerConnection
import org.webrtc.RtpReceiver
import timber.log.Timber

open class PeerConnectionObserver : PeerConnection.Observer {

    override fun onIceCandidate(iceCandidate: IceCandidate?) {
        Timber.d("onIceCandidate")
    }

    override fun onDataChannel(dataChannel: DataChannel?) {
        Timber.d("onDataChannel")
    }

    override fun onIceConnectionReceivingChange(p0: Boolean) {
        Timber.d("onIceConnectionReceivingChange")
    }

    override fun onIceConnectionChange(state: PeerConnection.IceConnectionState?) {
        Timber.d("onIceConnectionChange : $state")
    }

    override fun onIceGatheringChange(state: PeerConnection.IceGatheringState?) {
        Timber.d("onIceGatheringChange : $state")
    }

    override fun onAddStream(mediaStream: MediaStream?) {
        Timber.d("onAddStream")
    }

    override fun onSignalingChange(p0: PeerConnection.SignalingState?) {
        Timber.d("onSignalingChange")
    }

    override fun onIceCandidatesRemoved(p0: Array<out IceCandidate>?) {
        Timber.d("onIceCandidatesRemoved")
    }

    override fun onRemoveStream(p0: MediaStream?) {
        Timber.d("onRemoveStream")
    }

    override fun onRenegotiationNeeded() {
        Timber.d("onRenegotiationNeeded")
    }

    override fun onAddTrack(receiver: RtpReceiver?, mediaStreamArray: Array<out MediaStream>?) {
        Timber.d("onAddTrack")
    }
}