package com.nix.sample.webrtc.mvp.exception

/**
 * Exception is thrown when we try to interact with a view, that is not attached to presenter.
 */
class ViewNotAttachedException : BaseException()
