package com.nix.sample.webrtc.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.nix.sample.webrtc.util.extensions.inflateView
import java.util.Objects

abstract class BaseListAdapter<T : Any>(
    areItemsTheSame: (T, T) -> Boolean = Objects::equals,
    areContentsTheSame: (T, T) -> Boolean = Objects::equals,
    callback: DiffUtil.ItemCallback<T> = DefaultDiffUtilItemCallback(
        areItemsTheSame,
        areContentsTheSame
    )
) : ListAdapter<T, BaseViewHolder<T>>(callback) {

    private val items: MutableList<T> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val view = parent.inflateView(getLayoutId(viewType))
        return onCreateViewHolder(view, viewType)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(items[position])
    }

    fun replaceItems(newItems: List<T>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun getItems(): List<T> = items

    override fun getItemCount() = items.size

    @LayoutRes abstract fun getLayoutId(viewType: Int): Int

    abstract fun onCreateViewHolder(view: View, viewType: Int): BaseViewHolder<T>
}