package com.nix.sample.webrtc.util.logger

import android.util.Log
import timber.log.Timber

class CrashReportingTree : Timber.Tree() {

    override fun log(
        priority: Int,
        tag: String?,
        message: String,
        throwable: Throwable?
    ) {
        if (priority == Log.ERROR) {
            TODO() // "10/06/2019 https://gitlab.nixdev.co/android-research/mvppattern-kotlin/issues/17"
        }
    }
}

