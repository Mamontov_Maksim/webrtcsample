package com.nix.sample.webrtc.ui.adapter

import android.content.Context
import android.content.res.Resources
import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView

open class BaseViewHolder<T : Any>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected val context: Context get() = itemView.context

    protected val resources: Resources get() = context.resources

    lateinit var item: T
        private set

    @CallSuper open fun bind(item: T) {
        this.item = item
    }
}