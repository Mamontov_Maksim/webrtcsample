package com.nix.sample.webrtc.ui.toolbar

import android.app.Activity
import android.widget.ImageView
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.di.scope.ActivityScope
import com.nix.sample.webrtc.ui.view.AppBarLayoutStub
import kotlinx.android.synthetic.main.toolbar_collapsing.view.collapsingToolbar
import kotlinx.android.synthetic.main.toolbar_collapsing.view.toolbarImageView
import javax.inject.Inject

@ActivityScope
class CollapsingToolbarInterface @Inject constructor(activity: Activity) :
    BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_collapsing

    var toolbarImageView: ImageView? = null
        private set

    var collapsingToolbarLayout: CollapsingToolbarLayout? = null
        private set

    override fun setTitle(title: String) {
        collapsingToolbarLayout?.title = title
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        toolbarImageView = appBarLayoutStub.toolbarImageView
        collapsingToolbarLayout = appBarLayoutStub.collapsingToolbar
    }
}