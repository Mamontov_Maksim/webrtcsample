package com.nix.sample.webrtc.ui

import androidx.annotation.LayoutRes
import com.nix.sample.webrtc.ui.view.AppBarLayoutStub

interface ToolbarInterface {

    @get:LayoutRes val layoutId: Int

    fun invalidate()

    fun setTitle(title: String)

    fun bindViews(appBarLayoutStub: AppBarLayoutStub)
}