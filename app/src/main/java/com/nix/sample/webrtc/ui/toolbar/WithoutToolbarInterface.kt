package com.nix.sample.webrtc.ui.toolbar

import android.app.Activity
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.di.scope.ActivityScope
import com.nix.sample.webrtc.ui.view.AppBarLayoutStub
import javax.inject.Inject

@ActivityScope
class WithoutToolbarInterface @Inject constructor(activity: Activity) :
    BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_without_toolbar

    override fun setTitle(title: String) {
        //none
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        // none
    }
}