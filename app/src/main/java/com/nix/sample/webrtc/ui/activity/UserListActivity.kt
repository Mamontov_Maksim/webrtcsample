package com.nix.sample.webrtc.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.ui.BaseActivity
import com.nix.sample.webrtc.mvp.contract.UserListContract
import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.ui.adapter.UserListAdapter
import kotlinx.android.synthetic.main.activity_user_list.recyclerView

class UserListActivity :
    BaseActivity<UserListContract.View, UserListContract.Presenter>(),
    UserListContract.View {

    override val layoutId = R.layout.activity_user_list

    lateinit var adapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        adapter = UserListAdapter {
            presenter.onUserClick(it)
        }

        super.onCreate(savedInstanceState)

        recyclerView.adapter = adapter
    }

    override fun backToMainActivity(user: User) {
        val intent = Intent().putExtra(MainActivity.USER_RESULT_KEY, user)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        toolbarInterface.setVisible(false)
    }

    override fun showList(userList: List<User>) {
        userList.forEach {
            adapter.addItem(it)
        }
    }

    override fun getUserList(): List<User> = intent.getParcelableArrayListExtra(USER_LIST_KEY)

    companion object {
        const val USER_LIST_KEY = "USER_LIST_KEY"
    }
}
