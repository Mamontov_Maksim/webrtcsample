package com.nix.sample.webrtc.webrtc.observer

import com.google.gson.Gson
import com.nix.sample.webrtc.mvp.entity.Message
import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.webrtc.client.SignallingClient
import org.webrtc.SessionDescription
import timber.log.Timber


class ReceiverSdpObserver(
    private val userSender: User,
    private val userReceiver: User,
    private val signallingClient: SignallingClient
) : SdpConnectionObserver() {

    val gson = Gson()

    override fun onCreateSuccess(description: SessionDescription?) {
        val descriptionJson = gson.toJson(description)
        val message = Message(
            userSender = userSender,
            userReceiver = userReceiver,
            message = descriptionJson
        )
        Timber.d("create success: message = $message")
        signallingClient.send(message)
    }
}