package com.nix.sample.webrtc.webrtc.view

import android.content.Context
import android.util.AttributeSet
import org.webrtc.EglBase
import org.webrtc.RendererCommon
import org.webrtc.RendererCommon.GlDrawer
import org.webrtc.RendererCommon.RendererEvents
import org.webrtc.SurfaceEglRenderer
import org.webrtc.SurfaceViewRenderer

class SurfaceRenderer(context: Context, attributeSet: AttributeSet) :
    SurfaceViewRenderer(context, attributeSet) {

    private var isInit = false

    override fun init(sharedContext: EglBase.Context?, rendererEvents: RendererEvents?) {
        if (!isInit) {
            super.init(sharedContext, rendererEvents)
            isInit = true
        }
    }

    override fun init(
        sharedContext: EglBase.Context?,
        rendererEvents: RendererEvents?,
        configAttributes: IntArray?,
        drawer: GlDrawer?
    ) {
        if (!isInit) {
            super.init(sharedContext, rendererEvents, configAttributes, drawer)
            isInit = true
        }
    }
}