package com.nix.sample.webrtc.mvp.presenter

import com.google.gson.Gson
import com.nix.sample.webrtc.mvp.BasePresenter
import com.nix.sample.webrtc.mvp.contract.MainActivityContract
import com.nix.sample.webrtc.mvp.entity.Message
import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.webrtc.client.PeerConnectionClient
import com.nix.sample.webrtc.webrtc.client.SignallingClient
import com.nix.sample.webrtc.webrtc.listener.SignallingClientListener
import com.nix.sample.webrtc.webrtc.observer.ReceiverSdpObserver
import com.nix.sample.webrtc.webrtc.observer.PeerConnectionObserver
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.webrtc.IceCandidate
import org.webrtc.MediaStream
import org.webrtc.PeerConnection.IceConnectionState
import org.webrtc.SessionDescription
import org.webrtc.SurfaceViewRenderer
import ru.terrakok.cicerone.Router
import java.lang.Exception
import java.util.UUID
import javax.inject.Inject

class MainPresenter @Inject constructor(
    router: Router,
    private val peerClient: PeerConnectionClient
) : BasePresenter<MainActivityContract.View>(router),
    MainActivityContract.Presenter {

    private lateinit var localUser: User
    private lateinit var currentUser: User

    private val signallingClient = SignallingClient(MainSignallingClientListener())
    private val gson = Gson()
    private var freePeerConnectionPosition = 0

    override fun onAttach() {
        initPeerClient()
        signIn()
        setCurrentResolutions()
    }

    private fun initPeerClient() {
        peerClient.startLocalVideoCapture(getMainSurfaceView())
    }

    private fun signIn() {
        val userName = UUID.randomUUID().toString()
        localUser = User(userName)

        signallingClient.signIn(localUser)

        view?.setUserNameText(userName)
        view?.addUser(localUser)
        view?.addHideUser(localUser)
    }

    override fun onCallClick() {
        signallingClient.getUsers(localUser)
    }

    override fun callUser(user: User) {
        view?.showProgress()
        addUserConnected(user)
        peerClient.call(
            freePeerConnectionPosition, getReceiverSdpObserver(
                userSender = localUser,
                userReceiver = user
            )
        )
    }

    override fun onSwitchCameraClick() {
        peerClient.switchCamera()
        setCurrentResolutions()
    }

    override fun onCameraResolutionSelected(resolution: String) {
        val width = resolution.split("x")[0].toInt()
        val height = resolution.split("x")[1].toInt()
        //TODO fix fps
        peerClient.changeResolution(width, height, 60)
    }

    override fun onRemoteSurfaceViewClick(surfaceView: SurfaceViewRenderer, peerConnectionId: Int) {
        val user = view?.getUser(peerConnectionId)!!
        if (user == localUser) {
            peerClient.changeLocalVideoTrackSink(surfaceView, getMainSurfaceView())
            peerClient.changeRemoteVideoTrackSink(
                peerConnectionId,
                getMainSurfaceView(),
                getSurfaceViewByUser(currentUser)
            )
            currentUser = localUser
        } else {
            if (currentUser == localUser) {
                peerClient.changeLocalVideoTrackSink(
                    getMainSurfaceView(),
                    getSurfaceViewByUser(currentUser)
                )
                peerClient.changeRemoteVideoTrackSink(
                    peerConnectionId - 1,
                    surfaceView,
                    getMainSurfaceView()
                )
                currentUser = user
            } else {
                peerClient.changeRemoteVideoTrackSink(
                    peerConnectionId - 1,
                    surfaceView,
                    getMainSurfaceView()
                )
                peerClient.changeRemoteVideoTrackSink(
                    view?.getUserPosition(currentUser)!! - 1,
                    getMainSurfaceView(),
                    getSurfaceViewByUser(currentUser)
                )
                currentUser = user
            }
        }
        view?.addHideUser(currentUser)
    }

    override fun initSurfaceView(surfaceView: SurfaceViewRenderer) {
        peerClient.initSurfaceView(surfaceView)
    }

    override fun onDetach() {
        signallingClient.destroy()
        peerClient.destroy()
    }

    override fun onDropCallClick() {
        peerClient.dropCall()
        if (currentUser != localUser) {
            peerClient
                .changeLocalVideoTrackSink(getSurfaceViewByUser(localUser), getMainSurfaceView())
            currentUser = localUser
        }
        view?.replaceAllUser(listOf(localUser))
        view?.addHideUser(localUser)
        freePeerConnectionPosition = 0

        view?.getUsers()?.forEach {
            signallingClient
                .send(Message(userReceiver = it, userSender = localUser, message = "disconnect"))
        }
    }

    private fun setCurrentResolutions() =
        view?.setCameraResolutionListSpinner(getCurrentCameraResolutions())

    private fun getCurrentCameraName() =
        peerClient.getCurrentCameraName()

    private fun getCurrentCameraResolutions() =
        peerClient.getCamera(getCurrentCameraName()).resolution

    private fun createPeerConnection(userConnected: User) {
        peerClient.newPeerConnection(MainPeerConnectionObserver(userConnected), userConnected)
        freePeerConnectionPosition = peerConnectionSize() - 1
    }

    private fun peerConnectionSize() = peerClient.getPeerConnectionSize()

    private fun addUserConnected(userConnected: User) {
        view?.addUser(userConnected)
        createPeerConnection(userConnected)
    }

    private fun getMainSurfaceView() = view?.getMainSurfaceView()!!

    private fun getSurfaceViewByUser(user: User) = view?.getSurfaceViewByUser(user)!!

    private fun onAddStream() {
        launch {
            view?.getRemoteSurfaceViewList()?.let {
                if (it.size == 2) {
                    peerClient.changeLocalVideoTrackSink(view?.getMainSurfaceView()!!, it[0])
                    peerClient.changeRemoteVideoTrackSink(
                        freePeerConnectionPosition, it[1], view?.getMainSurfaceView()!!
                    )
                    currentUser = view?.getUser(freePeerConnectionPosition + 1)!!
                    view?.addHideUser(currentUser)
                }
            }
        }
    }

    private fun getReceiverSdpObserver(userSender: User, userReceiver: User) =
        ReceiverSdpObserver(
            userSender = userSender,
            userReceiver = userReceiver,
            signallingClient = signallingClient
        )

    inner class MainPeerConnectionObserver(val userConnected: User) : PeerConnectionObserver() {

        override fun onIceCandidate(iceCandidate: IceCandidate?) {
            super.onIceCandidate(iceCandidate)
            val iceCandidateJson = gson.toJson(iceCandidate)
            val message = Message(
                userSender = localUser,
                userReceiver = userConnected,
                message = iceCandidateJson
            )
            signallingClient.send(message)
            peerClient.addIceCandidate(freePeerConnectionPosition, iceCandidate)
        }

        override fun onAddStream(mediaStream: MediaStream?) {
            super.onAddStream(mediaStream)
            try {
                view?.getRemoteSurfaceView(freePeerConnectionPosition + 1)?.let { surfaceView ->
                    launch {
                        val videoTrack = mediaStream?.videoTracks?.get(0)
                        videoTrack?.let {
                            it.addSink(surfaceView)
                            peerClient.setRemoteVideoTrack(freePeerConnectionPosition, it)
                        }
                    }
                }
                onAddStream()
            } catch (e: Exception) {
                view?.onError(e)
            }
        }

        override fun onIceConnectionChange(state: IceConnectionState?) {
            super.onIceConnectionChange(state)

            when (state) {

                IceConnectionState.FAILED -> view?.onError(Exception("Error in iceConnection"))
            }
        }
    }

    inner class MainSignallingClientListener : SignallingClientListener {

        override fun onConnectionEstablished() {
            view?.successfulConnected()
        }

        override fun onOfferReceived(description: SessionDescription, userSender: User) {
            launch {
                addUserConnected(userSender)
                view?.hideProgress()
                delay(500)
                peerClient.onRemoteSessionReceived(freePeerConnectionPosition, description)
                peerClient.answer(
                    freePeerConnectionPosition,
                    getReceiverSdpObserver(userSender = localUser, userReceiver = userSender)
                )
            }
        }

        override fun onAnswerReceived(description: SessionDescription, userSender: User) {
            launch {
                peerClient.onRemoteSessionReceived(freePeerConnectionPosition, description)
                view?.hideProgress()
            }
        }

        override fun onIceCandidateReceived(iceCandidate: IceCandidate) {
            peerClient.addIceCandidate(freePeerConnectionPosition, iceCandidate)
        }

        override fun onFailureConnect(exception: Exception) {
            view?.let { onError(exception) }
        }

        override fun onGetUsers(users: List<User>) {
            val arrayList = arrayListOf<User>()
            arrayList.addAll(users)
            view?.showUserListActivityForResult(arrayList)
        }

        override fun onDisconnect(userSender: User) {
        }
    }
}