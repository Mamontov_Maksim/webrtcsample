package com.nix.sample.webrtc.di.module.activity

import android.app.Activity
import com.nix.sample.webrtc.di.scope.ActivityScope
import com.nix.sample.webrtc.mvp.contract.UserListContract
import com.nix.sample.webrtc.mvp.presenter.UserListPresenter
import com.nix.sample.webrtc.ui.MessageInterface
import com.nix.sample.webrtc.ui.activity.UserListActivity
import com.nix.sample.webrtc.ui.dialog.AlertMessageInterface
import com.nix.sample.webrtc.ui.navigation.BaseNavigator
import com.nix.sample.webrtc.ui.navigation.UserListActivityNavigator
import dagger.Binds
import dagger.Module

@Module interface UserListActivityModule {

    @Binds @ActivityScope
    fun bindAlertMessageInterface(messageInterface: AlertMessageInterface): MessageInterface

    @Binds @ActivityScope
    fun bindNavigator(navigator: UserListActivityNavigator): BaseNavigator

    @Binds @ActivityScope
    fun bindPresenter(presenter: UserListPresenter): UserListContract.Presenter

    @Binds @ActivityScope
    fun bindActivity(activity: UserListActivity): Activity
}
