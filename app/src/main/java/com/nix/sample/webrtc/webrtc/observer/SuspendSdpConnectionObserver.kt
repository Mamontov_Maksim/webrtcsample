package com.nix.sample.webrtc.webrtc.observer

import com.nix.sample.webrtc.mvp.entity.Message
import org.webrtc.SessionDescription
import java.lang.Exception
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

open class SuspendSdpConnectionObserver(protected val continuation: Continuation<Message?>) :
    SdpConnectionObserver() {

    override fun onSetFailure(exception: String?) {
        super.onSetFailure(exception)
        continuation.resumeWithException(Exception(exception))
    }

    override fun onSetSuccess() {
        super.onSetSuccess()
        continuation.resume(null)
    }

    override fun onCreateSuccess(description: SessionDescription?) {
        super.onCreateSuccess(description)
        continuation.resume(null)
    }

    override fun onCreateFailure(exception: String?) {
        super.onCreateFailure(exception)
        continuation.resumeWithException(Exception(exception))
    }
}