package com.nix.sample.webrtc.di.module

import com.nix.sample.webrtc.di.scope.ActivityScope

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module class NavigationModule {

    private val cicerone: Cicerone<Router> = Cicerone.create(Router())

    @Provides @ActivityScope
    fun provideRouter(): Router = cicerone.router

    @Provides @ActivityScope
    fun provideNavigatorHolder(): NavigatorHolder = cicerone.navigatorHolder
}
