package com.nix.sample.webrtc.mvp.presenter

import com.nix.sample.webrtc.mvp.BasePresenter
import com.nix.sample.webrtc.mvp.contract.UserListContract
import com.nix.sample.webrtc.mvp.entity.User

import javax.inject.Inject
import ru.terrakok.cicerone.Router

class UserListPresenter @Inject constructor(
    router: Router
) : BasePresenter<UserListContract.View>(router),
    UserListContract.Presenter {

    override fun onAttach() {
        view?.getUserList()?.let {
            view?.showList(it)
        }
    }

    override fun onUserClick(user: User) {
        view?.backToMainActivity(user)
    }
}
