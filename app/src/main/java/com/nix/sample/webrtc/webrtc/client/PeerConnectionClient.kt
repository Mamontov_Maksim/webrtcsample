package com.nix.sample.webrtc.webrtc.client

import android.content.Context
import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.webrtc.camera.CameraEnumeratorHelper
import com.nix.sample.webrtc.webrtc.camera.VideoCapturerHelper
import com.nix.sample.webrtc.webrtc.observer.PeerConnectionObserver
import com.nix.sample.webrtc.webrtc.observer.SdpConnectionObserver
import org.webrtc.EglBase
import org.webrtc.EglBase.CONFIG_RECORDABLE
import org.webrtc.GlRectDrawer
import org.webrtc.HardwareVideoDecoderFactory
import org.webrtc.HardwareVideoEncoderFactory
import org.webrtc.IceCandidate
import org.webrtc.MediaConstraints
import org.webrtc.MediaStream
import org.webrtc.PeerConnection
import org.webrtc.PeerConnectionFactory
import org.webrtc.SdpObserver
import org.webrtc.SessionDescription
import org.webrtc.SurfaceTextureHelper
import org.webrtc.SurfaceViewRenderer
import org.webrtc.VideoCapturer
import org.webrtc.VideoTrack
import java.lang.Exception

@Deprecated("use PeerConnectionClient2")
class PeerConnectionClient(context: Context) {

    private val cameraSupporter by lazy { CameraEnumeratorHelper(context) }
    private val peerConnectionFactory by lazy { buildPeerConnectionFactory() }
    private val videoSource by lazy { peerConnectionFactory.createVideoSource(false) }
    private val peerConnectList = arrayListOf<PeerConnect>()
    private val cameraList by lazy { cameraSupporter.getCameraList() }

    private val rootEglBase: EglBase = EglBase.create()

    private lateinit var videoCapturer: VideoCapturerHelper
    private lateinit var localVideoTrack: VideoTrack
    private lateinit var mediaStream: MediaStream

    private val iceServer = listOf(
        PeerConnection.IceServer.builder("stun:stun.l.google.com:19302").createIceServer(),
        PeerConnection.IceServer.builder("stun:stun1.l.google.com:19302").createIceServer(),
        PeerConnection.IceServer.builder("stun:stun2.l.google.com:19302").createIceServer(),
        PeerConnection.IceServer.builder("stun:stun3.l.google.com:19302").createIceServer(),
        PeerConnection.IceServer.builder("stun:stun4.l.google.com:19302").createIceServer()
    )

    init {
        initPeerConnectionFactory(context)
        initVideoCapturer()
    }

    fun newPeerConnection(observer: PeerConnectionObserver, userConnected: User) =
        peerConnectionFactory.createPeerConnection(iceServer, observer)?.let { peerConnection ->
            peerConnection.addStream(mediaStream)
            val peer = PeerConnect(peerConnection, userConnected)
            peerConnectList.add(peer)
            peerConnection
        } ?: throw Exception("didn't create peer connection")

    fun initSurfaceView(view: SurfaceViewRenderer) = view.also { view ->
        view.setMirror(true)
        view.setEnableHardwareScaler(true)
        view.init(rootEglBase.eglBaseContext, null, CONFIG_RECORDABLE, GlRectDrawer())
    }

    fun startLocalVideoCapture(localVideoOutput: SurfaceViewRenderer) {
        initSurfaceView(localVideoOutput)

        val surfaceTextureHelper =
            SurfaceTextureHelper.create(Thread.currentThread().name, rootEglBase.eglBaseContext)
        (videoCapturer as VideoCapturer).initialize(
            surfaceTextureHelper,
            localVideoOutput.context,
            videoSource.capturerObserver
        )

        videoCapturer.startCapture(VIDEO_CAPTURE_WIDTH, VIDEO_CAPTURE_HEIGHT, VIDEO_CAPTURE_FPS)

        localVideoTrack = peerConnectionFactory.createVideoTrack(LOCAL_TRACK_ID, videoSource)
        localVideoTrack.addSink(localVideoOutput)

        mediaStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID)
        mediaStream.addTrack(localVideoTrack)
    }

    fun addIceCandidate(peerConnectionId: Int, iceCandidate: IceCandidate?) =
        peerConnectList[peerConnectionId].peerConnection.addIceCandidate(iceCandidate)

    fun call(peerConnectionId: Int, sdpObserver: SdpObserver) =
        peerConnectList[peerConnectionId].peerConnection.call(sdpObserver)

    fun answer(peerConnectionId: Int, sdpObserver: SdpObserver) =
        peerConnectList[peerConnectionId].peerConnection.answer(sdpObserver)

    fun onRemoteSessionReceived(peerConnectionId: Int, sessionDescription: SessionDescription) =
        peerConnectList[peerConnectionId].peerConnection
            .setRemoteDescription(SdpConnectionObserver(), sessionDescription)

    fun switchCamera() =
        videoCapturer.switchCamera(null)

    fun changeResolution(width: Int, height: Int, fps: Int) =
        videoCapturer.changeCaptureFormat(width, height, fps)

    fun getCurrentCameraName() = videoCapturer.getCurrentCameraName()

    fun changeLocalVideoTrackSink(
        surfaceChangered: SurfaceViewRenderer,
        surfaceChanger: SurfaceViewRenderer
    ) {
        localVideoTrack.removeSink(surfaceChangered)
        localVideoTrack.addSink(surfaceChanger)
    }

    fun setRemoteVideoTrack(peerConnectionId: Int, videoTrack: VideoTrack) {
        peerConnectList[peerConnectionId].videoTrack = videoTrack
    }

    fun changeRemoteVideoTrackSink(
        peerConnectionId: Int,
        surfaceChangered: SurfaceViewRenderer,
        surfaceChanger: SurfaceViewRenderer
    ) {
        peerConnectList[peerConnectionId].videoTrack?.let {
            it.removeSink(surfaceChangered)
            it.addSink(surfaceChanger)
        }
    }

    fun getCamera(cameraName: String) =
        cameraList.find { it.name == cameraName } ?: throw Exception("didn't found camera")

    fun getPeerConnectionSize() = peerConnectList.size

    fun dropCall() {
        peerConnectList.forEach {
            it.peerConnection.close()
        }
        peerConnectList.removeAll(peerConnectList)
    }

    fun destroy() {
        videoCapturer.dispose()
        videoSource.dispose()
        peerConnectList.forEach { it.peerConnection.close() }
        mediaStream.dispose()
        peerConnectionFactory.dispose()
        rootEglBase.release()
    }

    private fun initPeerConnectionFactory(context: Context) {
        val options =
            PeerConnectionFactory.InitializationOptions.builder(context)
                .apply {
                    setEnableInternalTracer(true)
                    setFieldTrials("WebRTC-H264HighProfile/Enabled/")
                }.createInitializationOptions()
        PeerConnectionFactory.initialize(options)
    }

    private fun buildPeerConnectionFactory(): PeerConnectionFactory {
        return PeerConnectionFactory.builder().apply {
            setVideoDecoderFactory(getDefaultVideoDecoder())
            setVideoEncoderFactory(getDefaultVideoEncoder())
            setOptions(PeerConnectionFactory.Options().apply {
                disableEncryption = true
                disableNetworkMonitor = true
            })
        }.createPeerConnectionFactory()
    }

    private fun initVideoCapturer() =
        cameraSupporter.run {
            deviceNames.find {
                isFrontFacing(it)
            }?.let {
                videoCapturer = createCapturer(it, null)
            } ?: throw IllegalStateException()
        }

    private fun PeerConnection.call(sdpObserver: SdpObserver) {
        createOffer(object : SdpObserver by sdpObserver {
            override fun onCreateSuccess(sessionDescription: SessionDescription?) {
                setLocalDescription(SdpConnectionObserver(), sessionDescription)
                sdpObserver.onCreateSuccess(sessionDescription)
            }
        }, getMediaConstraints())
    }

    private fun PeerConnection.answer(sdpObserver: SdpObserver) {
        createAnswer(object : SdpObserver by sdpObserver {
            override fun onCreateSuccess(sessionDescription: SessionDescription?) {
                setLocalDescription(SdpConnectionObserver(), sessionDescription)
                sdpObserver.onCreateSuccess(sessionDescription)
            }
        }, getMediaConstraints())
    }

    private fun getMediaConstraints() = MediaConstraints().apply {
        mandatory.add(MediaConstraints.KeyValuePair(OFFER_TO_RECEIVE_VIDEO, TRUE_STRING))
    }

    private fun getDefaultVideoDecoder() =
        HardwareVideoDecoderFactory(rootEglBase.eglBaseContext)

    private fun getDefaultVideoEncoder() =
        HardwareVideoEncoderFactory(rootEglBase.eglBaseContext, true, true)

    companion object {

        private const val LOCAL_TRACK_ID = "local_track"
        private const val LOCAL_STREAM_ID = "local_track"

        private const val OFFER_TO_RECEIVE_VIDEO = "OfferToReceiveVideo"
        private const val TRUE_STRING = true.toString()

        private const val VIDEO_CAPTURE_WIDTH = 1920
        private const val VIDEO_CAPTURE_HEIGHT = 1080
        private const val VIDEO_CAPTURE_FPS = 60
    }
}