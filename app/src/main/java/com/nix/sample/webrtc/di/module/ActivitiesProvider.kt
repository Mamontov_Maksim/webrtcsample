package com.nix.sample.webrtc.di.module

import com.nix.sample.webrtc.di.module.activity.UserListActivityFragmentProvider
import com.nix.sample.webrtc.di.module.activity.UserListActivityModule
import com.nix.sample.webrtc.ui.activity.UserListActivity

import com.nix.sample.webrtc.di.module.activity.MainActivityModule
import com.nix.sample.webrtc.di.module.rtc.WebRTCModule
import com.nix.sample.webrtc.di.scope.ActivityScope
import com.nix.sample.webrtc.ui.activity.MainActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class ActivitiesProvider {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            UserListActivityFragmentProvider::class,
            UserListActivityModule::class,
            NavigationModule::class
        ]
    )
    abstract fun contributesUserListActivityInjector(): UserListActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            MainActivityModule::class,
            WebRTCModule::class,
            NavigationModule::class
        ]
    )
    abstract fun contributeMainActivityInjector(): MainActivity
}
