package com.nix.sample.webrtc.ui.toolbar

import android.app.Activity
import com.google.android.material.tabs.TabLayout
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.di.scope.ActivityScope
import com.nix.sample.webrtc.ui.view.AppBarLayoutStub
import kotlinx.android.synthetic.main.toolbar_collapsing.view.toolbar
import kotlinx.android.synthetic.main.toolbar_tabs.view.tabLayout
import javax.inject.Inject

@ActivityScope
class TabCollapsingToolbarInterface @Inject constructor(activity: Activity) :
    BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_tabs_collapsing

    var tabLayout: TabLayout? = null
        private set

    override fun setTitle(title: String) {
        toolbar?.title = title
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        toolbar = appBarLayoutStub.toolbar
        tabLayout = appBarLayoutStub.tabLayout
    }
}