package com.nix.sample.webrtc.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.nix.sample.webrtc.util.extensions.inflateView
import timber.log.Timber

abstract class BaseAdapter<T : Any> : RecyclerView.Adapter<BaseViewHolder<T>>() {

    private val items: MutableList<T> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val view = parent.inflateView(getLayoutId(viewType))
        return onCreateViewHolder(view, viewType)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(items[position])
    }

    // TODO: 01/07/2019 Implement DiffUtil https://gitlab.nixdev.co/android-research/mvppattern-kotlin/issues/57
    fun replaceItems(newItems: List<T>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun removeItem(item: T) {
        val position = items.indexOf(item)
        if (position != -1) {
            items.removeAt(position)
            notifyItemRemoved(position)
        } else {
            Timber.e("List doesn't contain such item")
        }
    }

    fun addItem(item: T) {
        items.add(item)
        notifyItemInserted(items.lastIndex)
    }

    fun addItems(items: List<T>) {
        this.items.addAll(items)
        notifyItemRangeInserted(this.items.size - items.size, items.size)
    }

    fun replaceItem(item: T) {
        val position = items.indexOf(item)
        if (position != -1) {
            items[position] = item
            notifyItemChanged(position)
        } else {
            Timber.e("List doesn't contain such item")
        }
    }

    fun getItems(): List<T> = items

    override fun getItemCount() = items.size

    @LayoutRes abstract fun getLayoutId(viewType: Int): Int

    abstract fun onCreateViewHolder(view: View, viewType: Int): BaseViewHolder<T>
}