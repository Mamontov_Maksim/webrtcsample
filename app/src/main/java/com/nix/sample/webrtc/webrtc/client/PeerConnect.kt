package com.nix.sample.webrtc.webrtc.client

import com.nix.sample.webrtc.mvp.entity.User
import org.webrtc.PeerConnection
import org.webrtc.VideoTrack

data class PeerConnect(
    val peerConnection: PeerConnection,
    val userConnected: User,
    var videoTrack: VideoTrack? = null
)