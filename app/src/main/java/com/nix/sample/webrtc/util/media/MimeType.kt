package com.nix.sample.webrtc.util.media

object MimeType {
    const val PLAIN_TEXT = "text/plain"
}