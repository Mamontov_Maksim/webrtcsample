package com.nix.sample.webrtc.mvp.contract

import com.nix.sample.webrtc.mvp.BaseContract
import com.nix.sample.webrtc.mvp.entity.User

interface UserListContract {

    interface View : BaseContract.View {

        fun getUserList(): List<User>

        fun showList(userList: List<User>)

        fun backToMainActivity(user: User)
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onUserClick(user: User)
    }
}
