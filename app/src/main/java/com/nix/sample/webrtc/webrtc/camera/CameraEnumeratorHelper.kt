package com.nix.sample.webrtc.webrtc.camera

import android.content.Context
import android.content.Context.CAMERA_SERVICE
import android.hardware.camera2.CameraManager
import org.webrtc.Camera2Enumerator
import org.webrtc.CameraVideoCapturer.CameraEventsHandler

class CameraEnumeratorHelper(val context: Context) : Camera2Enumerator(context) {

    protected val cameraManager by lazy { context.getSystemService(CAMERA_SERVICE) as CameraManager }

    fun getCamera(cameraName: String): Camera {
        val widthList = mutableListOf<Int>()
        val heightList = mutableListOf<Int>()
        val fpsList = mutableListOf<Int>()
        val resolution = mutableListOf<String>()

        getSupportedFormats(cameraName)?.forEach {
            widthList.add(it.width)
            heightList.add(it.height)
            fpsList.add(it.framerate.max)
            resolution.add("${it.width}x${it.height}")
        }

        return Camera(
            name = cameraName,
            widthList = widthList,
            heightList = heightList,
            fpsList = fpsList,
            resolution = resolution
        )
    }

    fun getCameraList() = deviceNames.map { getCamera(it) }

    override fun createCapturer(deviceName: String?, eventsHandler: CameraEventsHandler?) =
        VideoCapturerHelper(context, deviceName, eventsHandler)
}

