package com.nix.sample.webrtc.mvp.repository.server

import com.nix.sample.webrtc.mvp.entity.User
import com.nix.sample.webrtc.mvp.repository.server.contract.ServerRepositoryContract
import java.lang.Exception
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class UsersObserver(private val continuation: Continuation<List<User>>) : ServerRepositoryContract.GetUsersObserver {

    override fun onGetUsersSuccess(users: List<User>) {
        continuation.resume(users)
    }

    override fun onGetUsersFailed(exception: Exception) {
        continuation.resumeWithException(exception)
    }
}