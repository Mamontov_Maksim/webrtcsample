package com.nix.sample.webrtc

import com.nix.sample.webrtc.di.component.DaggerAppComponent
import com.nix.sample.webrtc.util.logger.CrashReportingTree
import com.nix.sample.webrtc.util.test.OpenForTesting
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber

@OpenForTesting class Application : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        setupTimber()
        setupRxJavaPlugins()
    }

    private fun setupTimber() = Timber.plant(CrashReportingTree(), Timber.DebugTree())

    private fun setupRxJavaPlugins() {
        RxJavaPlugins.setErrorHandler {
            @Suppress("ConstantConditionIf")
            if (BuildConfig.DEBUG) {
                Thread
                    .currentThread()
                    .uncaughtExceptionHandler
                    .uncaughtException(Thread.currentThread(), it)
            } else Timber.e(it, "Error without subscriber")
        }
    }

    override fun applicationInjector(): AndroidInjector<Application> =
        DaggerAppComponent.builder().context(this).build()
}