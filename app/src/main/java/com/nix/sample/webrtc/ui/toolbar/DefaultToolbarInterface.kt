package com.nix.sample.webrtc.ui.toolbar

import android.app.Activity
import androidx.core.view.isVisible
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.di.scope.ActivityScope
import com.nix.sample.webrtc.ui.view.AppBarLayoutStub
import kotlinx.android.synthetic.main.toolbar_collapsing.view.toolbar
import javax.inject.Inject

@ActivityScope
class DefaultToolbarInterface @Inject constructor(activity: Activity) :
    BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_default

    override fun setTitle(title: String) {
        toolbar?.title = title
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        toolbar = appBarLayoutStub.toolbar
    }

    fun setVisible(visible: Boolean) {
        toolbar?.isVisible = visible
    }
}