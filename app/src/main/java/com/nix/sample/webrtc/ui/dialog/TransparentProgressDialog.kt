package com.nix.sample.webrtc.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.nix.sample.webrtc.R
import com.nix.sample.webrtc.util.extensions.inflateView

open class TransparentProgressDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?) =
        super
            .onCreateDialog(savedInstanceState)
            .apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = container?.inflateView(R.layout.dialog_transparent_progress)

    companion object {

        fun newInstance() = TransparentProgressDialog()
    }
}